# UnBound

The UnBound engine, currently in development.  The goal is to recreate MOTHER 1 (using the original calculations for things like battles and leveling up), except it looks like MOTHER 3, by extracting the original map and text from the ROM.  Eventually it might be able to play MOTHER 3 as well.

Right now, it has a fully working battle system that works for regular attacks, defending and checking, by scanning the enemies from the ROM.  It also has the battle memory so you can pick any enemy in the game, and can calculate experience points and level up a character.

**This game needs pixel artists and musicians!**  I could also use help with ROM hacking as I haven't done it before and am not very good at it.  If you'd like to help, just let me know.

## Screenshots

These are going to change, but here's what I have so far:

![Menu](Screenshots/menu.png)

![Attack options](Screenshots/attackoptions.png)

![Enemy attacking](Screenshots/enemyattack.png)

![Battle memory](Screenshots/battlememory.png)
