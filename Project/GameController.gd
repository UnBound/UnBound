extends Node2D

signal change_color

var yaml := preload("res://addons/godot-yaml/gdyaml.gdns").new()

const Ally := preload("res://scripts/ally.gd")
const Enemy := preload("res://scripts/enemy.gd")

const option_names := {
	"attack": "Attack",
	"goods": "Goods",
	"ability": "Check",
	"psi": "PSI",
	"guard": "Guard",
	"run": "Run",
	"auto": "Auto fight",
	"exit": "Exit"
}

const ninten_stats := {
	"name": "Ken",
	"max_hp": 30,
	"max_pp": 8,
	"offense": 5,
	"defense": 5,
	"fight": 5,
	"speed": 5,
	"wisdom": 5,
	"strength": 5,
	"force": 5,
	"experience_points": 0,
	"level_up_stats": {
		"fight": 4,
		"speed": 4,
		"wisdom": 4,
		"strength": 4,
		"force": 4
	},
	"level_rate": 214
}

const ana_stats := {
	"name": "Ana",
	"max_hp": 26,
	"max_pp": 12,
	"offense": 1,
	"defense": 3,
	"fight": 1,
	"speed": 3,
	"wisdom": 7,
	"strength": 3,
	"force": 8,
	"experience_points": 0,
	"level_up_stats": {
		"fight": 0,
		"speed": 2,
		"wisdom": 6,
		"strength": 3,
		"force": 7
	},
	"level_rate": 203
}

const all_colors := {
	"Plain": [Color("#e7d6ce"), Color("#cebdb5")],
	"Mint": [Color("#e7f7ff"), Color("#cedee7")],
	"Strawberry": [Color("#ffcee7"), Color("#ffb5ce")],
	"Banana": [Color("#ffffb5"), Color("#f7e79c")],
	"Peanut": [Color("#ffdebd"), Color("#e7c6a5")],
	"Grape": [Color("#e7d6f7"), Color("#cebdde")],
	"Melon": [Color("#def7de"), Color("#c6dec6")]
}

const DIRECTION_UP = 0
const DIRECTION_UP_RIGHT = 1
const DIRECTION_RIGHT = 2
const DIRECTION_DOWN_RIGHT = 3
const DIRECTION_DOWN = 4
const DIRECTION_DOWN_LEFT = 5
const DIRECTION_LEFT = 6
const DIRECTION_UP_LEFT = 7
const DIRECTION_IN_PLACE = 8 # for teleporting

const color_order := ["Plain", "Mint", "Strawberry", "Banana", "Peanut", "Grape", "Melon"]

var current_scene = null

func _get_colors():
	return all_colors[chosen_color]

func _set_colors(_new_color) -> void:
	assert(false, "You may not set the colors attribute directly, see the documentation for adding a custom color")

func _get_chosen_color():
	return chosen_color

func _set_chosen_color(new_color) -> void:
	chosen_color = new_color
	emit_signal("change_color")

var debug_mode := true

var chosen_color = "Plain" setget _set_chosen_color, _get_chosen_color
var colors setget _set_colors, _get_colors

const BATTLE_TYPE_BATTLE_MEMORY := -1
const BATTLE_TYPE_NORMAL := 0
const BATTLE_TYPE_BOSS := 1
const BATTLE_TYPE_FINAL_BOSS := 2 # the final boss is going to be slightly different, probably only in the animation and sound effect when starting battle

const fight_scene_packed := preload("res://screens/fight/Fight.tscn")
const battle_memory_scene_packed := preload("res://screens/battlememory/BattleMemory.tscn")
var fight_scene
var battle_memory_scene
var allies := [Ally.new("Ken", ninten_stats, 0), Ally.new("Ana", ana_stats, 0)]
var enemies := []
var battle_type: int = BATTLE_TYPE_NORMAL
var enemy_stats: Array

var objects := []
var doors := []

func _parse_objects() -> void:
	var data_file := File.new()
	assert(data_file.open("res://gamedata/objectdata.yaml", File.READ) == OK, "opening file failed")
	var data_text = data_file.get_as_text()
	data_file.close()
	var data_parse = yaml.parse(data_text)
	assert(data_parse.error == OK, data_parse.error)
	objects = data_parse.result
	for object in objects:
		if object.type <= 4:
			doors.append(object)

func _parse_enemies() -> void:
	var data_file := File.new()
	assert(data_file.open("res://gamedata/enemies.yaml", File.READ) == OK, "opening file enemies failed")
	var data_text = data_file.get_as_text()
	data_file.close()
	var data_parse = yaml.parse(data_text)
	assert(data_parse.error == OK, data_parse.error)
	enemy_stats = data_parse.result

func collides(position: Vector2) -> bool:
	for object in objects:
		if object.position_x == position.x and object.position_y == position.y:
			return true
	return false

func get_enemy(name: String) -> Enemy:
	var stats
	for stat in enemy_stats:
		if stat["name"] == name:
			stats = stat
			break
	var parsed_stats = {"max_hp": stats.hp, "max_pp": stats.pp, "offense": stats.offense, "defense": stats.defense,
						"fight": stats.fight, "force": stats.force, "speed": stats.speed,
						"strength": stats.strength, "wisdom": stats.wisdom, "description": stats.description,
						"experience_points_drop": stats.experience, "money_drop": stats.money}
	return Enemy.new(name, parsed_stats)

func _init() -> void:
	randomize()

func _ready() -> void:
	_parse_enemies()
	_parse_objects()
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)
	chosen_color = "Plain"
	GlobalSignal.add_user_signal("end_battle")
	GlobalSignal.add_listener("end_battle", self, "end_battle")
	GlobalSignal.add_user_signal("change_color")
	GlobalSignal.add_emitter("change_color", self)
	GlobalSignal.emit_signal("change_color")

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("select"):
		_set_chosen_color(color_order[(color_order.find(chosen_color) + 1) % all_colors.size()])

func fight() -> void:
	#enemies.clear()
	#enemies.push_front(get_enemy("Lamp"))
	#enemies.push_front(get_enemy("Doll"))
	var _annoying = get_tree().change_scene_to(fight_scene_packed)
	fight_scene = fight_scene_packed.instance()

func back() -> void:
	var _annoying = goto_scene("res://screens/menu/Menu.tscn")

func goto_scene(path: String) -> SceneTree:
	var scene = ResourceLoader.load(path)
	var temp_scene = scene.instance()
	call_deferred("_goto_scene_deferred", temp_scene, current_scene)
	return temp_scene

func _goto_scene_deferred(scene, previous_scene) -> void:
	if is_instance_valid(previous_scene):
		previous_scene.free()
	get_tree().get_root().add_child(scene)
	get_tree().set_current_scene(scene)
	current_scene = scene

func battle_memory() -> void:
	battle_memory_scene = goto_scene("res://screens/battlememory/BattleMemory.tscn")
	battle_memory_scene.connect("exit", self, "back")

func end_battle() -> void:
	battle_memory()

func play_music(music: AudioStream) -> void:
	$Music.stream = music
	$Music.play()

func stop_music() -> void:
	$Music.stop()

static func replace_color(p_image: Image, p_old_c: Color, p_new_c: Color) -> void:
	if p_old_c == p_new_c:
		return

	p_image.lock()

	for y in p_image.get_height():
		for x in p_image.get_width():
			if p_image.get_pixel(x, y) == p_old_c:
				p_image.set_pixel(x, y, p_new_c)

	p_image.unlock()
