from PIL import Image

Image.MAX_IMAGE_PIXELS = None
im = Image.open("eb0map_big.png")

for h in range(0, im.height, 32):
     nim = im.crop((0, h, im.width-1, min(im.height, h + 32) - 1))
     nim.save("split/tile-" + str(h // 32) + ".png")
