extends NinePatchRect

var _label: Label

func _set_content(new_content: String) -> void:
	_label.text = new_content

func _get_content() -> String:
	return _label.text

var content: String setget _set_content, _get_content

func _ready() -> void:
	var background = ColorRect.new()
	background.mouse_filter = Control.MOUSE_FILTER_IGNORE
	background.rect_position = Vector2(3, 3)
	background.rect_size = rect_size - Vector2(6, 6)
	background.show_behind_parent = true
	background.color = Color("#000000")
	background.set_script(load("res://colored_background.gd"))
	add_child(background)

	_label = Label.new()
	_label.mouse_filter = Control.MOUSE_FILTER_IGNORE
	_label.name = "Text"
	_label.rect_position = Vector2(4, 5)
	_label.rect_size = rect_size - Vector2(6, 6)

	# I know that this isn't how it works in the game, but it looks better and probably would have been
	# if it would have been easier in GBA code
	_label.align = Label.ALIGN_CENTER

	_label.add_color_override("font_color", Color("#000000"))
	_label.theme = load("res://normal.tres")
	add_child(_label)
