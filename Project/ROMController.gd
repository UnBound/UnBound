extends Node2D

# this script is used to write data to the ROM

const NES_PPU_COLORS = [
	Color("#555555"), Color("#001773"), Color("#000786"), Color("#2e0578"), Color("#59024d"), Color("#720011"), Color("#6e0000"), Color("#4c0800"), # $00-07
	Color("#171b00"), Color("#002a00"), Color("#003100"), Color("#002e08"), Color("#002645"), Color("#000000"), Color("#000000"), Color("#000000"), # $08-0F
	Color("#a5a5a5"), Color("#0057c6"), Color("#223fe5"), Color("#6e28d9"), Color("#ae1aa6"), Color("#d21759"), Color("#d12107"), Color("#a73700"), # $10-17
	Color("#635100"), Color("#186700"), Color("#007200"), Color("#007331"), Color("#006a84"), Color("#000000"), Color("#000000"), Color("#000000"), # $18-1F
	Color("#feffff"), Color("#2fa8ff"), Color("#5d81ff"), Color("#9c70ff"), Color("#f772ff"), Color("#ff77bd"), Color("#ff7e75"), Color("#ff8a2b"), # $20-27
	Color("#cda000"), Color("#81b802"), Color("#3dc830"), Color("#12cd7b"), Color("#0dc5d0"), Color("#3c3c3c"), Color("#000000"), Color("#000000"), # $28-2F
	Color("#feffff"), Color("#a4deff"), Color("#b1c8ff"), Color("#ccbeff"), Color("#f4c2ff"), Color("#ffc5ea"), Color("#ffc7c9"), Color("#ffcdaa"), # $30-37
	Color("#efd696"), Color("#d0e095"), Color("#b3e7a5"), Color("#9feac3"), Color("#9ae8e6"), Color("#afafaf"), Color("#000000"), Color("#000000")  # $38-3F
]

const FilePicker: PackedScene = preload("res://screens/FilePicker.tscn")

var rom_path := "res://ebproto.nes"
var pending_objects := {} # the objects that are going to be written to the ROM

var sprite_palettes := []
var sprites := []

func _process(delta: float) -> void:
	#if Input.is_action_just_released("save"):
	#	save()
	if Input.is_action_just_released("open"):
		open()

func _get_bit(byte: int, index: int) -> int:
	return (byte & (1 << index)) >> index

func _slice_byte(byte: int, start = null, end = null) -> int:
	if start == null:
		start = 0
	var shifted_number: int = byte >> start
	var extracted_bits: int
	if end == null:
		extracted_bits = shifted_number
	else:
		var mask: int = (1 << end - start + 1) - 1
		extracted_bits = shifted_number & mask
	return extracted_bits

const _DEFAULT_PALETTE = [Color.transparent, Color.black, Color.darkgray, Color.white]

func create_handle(mode := File.READ) -> File:
	var handle := File.new()
	assert(handle.open(rom_path, mode) == OK, "opening ROM failed")
	return handle

func read_sprite_image(handle: File, tiles: Array,
						palette_1 := _DEFAULT_PALETTE, palette_2 := _DEFAULT_PALETTE) -> ImageTexture:
	var texture := ImageTexture.new()
	var image := Image.new()
	image.create(64, 64, false, Image.FORMAT_RGBA8)
	image.lock()
	var tile_index := 0
	for tile in tiles:
		var current_palette: Array
		if tile_index <= 1:
			current_palette = palette_1
		else:
			current_palette = palette_2
		var tile_x: int = tile.location[0] + 0x10
		var tile_y: int = tile.location[1]
		handle.seek(0x58010 + tile.ppu_index * 0x10)
		var ppu_bytes := []
		# dump the raw PPU bytes into an array that is parsed later
		for byte in range(8 * 8):
			ppu_bytes.append(handle.get_8())
		for row in range(8):
			for column in range(8):
				# this allows mirroring
				var real_row: int
				var real_column: int
				if tile["mirror_x"]:
					real_column = 7 - column
				else:
					real_column = column
				if tile["mirror_y"]:
					real_row = 7 - row
				else:
					real_row = row

				# if you want to understand this line, read up on how the NES PPU works
				var palette_index := _get_bit(ppu_bytes[row], 7 - column) + _get_bit(ppu_bytes[row + 8], 7 - column) * 2
				image.set_pixel(tile_x + real_column, tile_y + real_row, current_palette[palette_index])
		tile_index += 1
	image.unlock()
	texture.create_from_image(image, 0)
	return texture

func read_sprite_tiles(handle: File, sprite_index: int, pointer: int, ppu_offset: int) -> Array:
	handle.seek(0x2a010 + sprite_index)
	var tiles := []
	for tile in range(4):
		var current_tile := {}
		current_tile["location"] = [handle.get_8(), handle.get_8()]
		var attributes := handle.get_8()
		current_tile["ppu_index"] = handle.get_8() + ppu_offset
		current_tile["mirror_x"] = bool(_get_bit(attributes, 6))
		current_tile["mirror_y"] = bool(_get_bit(attributes, 7))
		tiles.append(current_tile)
	return tiles

func read_sprite(handle: File, sprite_index: int) -> ImageTexture:
	handle.seek(0x2a010 + sprite_index * 0x4)

	var pointer := handle.get_16()
	var ppu_offset := handle.get_8()
	if ppu_offset >= 0x80:
		ppu_offset += 0x180
	var attributes := handle.get_8()

	var palette_1: Array = sprite_palettes[_slice_byte(attributes, 0, 1)]
	var palette_2: Array = sprite_palettes[_slice_byte(attributes, 2, 3)]

	var tiles := read_sprite_tiles(handle, pointer - 0x8000, pointer, ppu_offset)

	var texture := read_sprite_image(handle, tiles, palette_1, palette_2)
	return texture

func read_sprite_palette(handle: File, palette_index: int, background = Color.transparent, replace_background := true) -> Array:
	handle.seek(0x29a15 + (palette_index * 0x4))
	var current_palette := []
	for color_index in range(4):
		if color_index == 0 and replace_background:
			handle.get_8()
			current_palette.append(background)
		else:
			current_palette.append(NES_PPU_COLORS[handle.get_8()])
	return current_palette

func get_sprite(sprite_index: int) -> ImageTexture:
	return sprites[sprite_index]

func _ready():
	var handle := create_handle()
	# cache the sprites in RAM to save a lot of time (otherwise it would constantly read from disk every time a sprite loaded
	for palette in range(6, 10):
		sprite_palettes.append(read_sprite_palette(handle, palette))
	for sprite_index in range(0x289 + 1):
		sprites.append(read_sprite(handle, sprite_index))
	handle.close()

func parse_object(object: Dictionary):
	"converts the object dictionary into raw bytes that can be written to the ROM"
	print("queued ", object)
	var generated_object := PoolByteArray()
	var generated_words := [] # 16-bit numbers that need to be converted to 8-bit
	generated_words.append(object.type + (object.position_x << 2))
	generated_words.append(object.direction + (object.position_y << 2) + 0x2000)
	if object.type <= 4:
		generated_words.append(object.music + (object.target_x << 2))
		generated_words.append(object.target_direction + (object.target_y << 2) + 0x2000)
		for word in generated_words:
			generated_object.append(word & 0x00FF)
			generated_object.append((word & 0xFF00) >> 8)
		#assert(generated_object != object.raw)
	else:
		print("not implemented")
		return null
	return generated_object

func edit_object(object: Dictionary):
	"queues the object to be written to the ROM"
	var parsed = parse_object(object)
	if parsed != null: # null = not implemented yet
		pending_objects[object.location] = parsed
	return parsed

func save_object(handle: File, location: int, object: Array) -> void:
	print("saving object ", object, " at ", location, " to ROM")
	handle.seek(location)
	handle.store_buffer(object)

func save_to_handle(handle: File) -> void:
	for location in pending_objects.keys():
		save_object(handle, location, pending_objects[location])
		pending_objects.erase(location)

func save() -> void:
	print("saving")
	var handle := File.new()
	assert(handle.open(rom_path, File.READ_WRITE) == OK, "opening ROM failed")
	save_to_handle(handle)
	handle.close()

var file_dialog: Popup

func open():
	if file_dialog != null:
		return
	file_dialog = FilePicker.instance()
	get_viewport().add_child(file_dialog)
	file_dialog.connect("selected", self, "_on_file_selected")
	file_dialog.popup()

func _on_file_selected(path: String):
	print(path)
	rom_path = path
	if (file_dialog != null):
		file_dialog.queue_free()
		file_dialog = null
