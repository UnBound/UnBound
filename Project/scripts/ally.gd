extends "res://scripts/character.gd"

func _set_experience_points(new_experience_points: int) -> void:
	# if I allowed this, it could cause level-downs, which aren't currently implemented and would mess things up
	assert(new_experience_points >= experience_points, "You cannot remove experience points from an ally")
	experience_points = new_experience_points
	_recalculate_experience_points()
func _get_experience_points() -> int:
	return experience_points
var experience_points: int setget _set_experience_points, _get_experience_points

func _set_pending_level_up(_new_pending_level_up: bool) -> void:
	assert(false, "You cannot set the pending level up directly for an ally, instead add to the level")
func _get_pending_level_up() -> bool:
	return level_target > level
var pending_level_up: bool setget _set_pending_level_up, _get_pending_level_up

func _set_level(new_level: int) -> void:
	_set_level_target(new_level)
	while _get_pending_level_up():
		var _annoying = level_up()
func _get_level() -> int:
	return level
var level: int = 1 setget _set_level, _get_level

func _set_level_target(new_level_target: int) -> void:
	assert(new_level_target >= level_target, "You cannot remove levels from an ally")
	if min(new_level_target, 99) > level_target:
		experience_points = total_experience_for_level(int(min(new_level_target, 99)))
		_recalculate_experience_points()
func _get_level_target() -> int:
	return level_target
var level_target: int setget _set_level_target, _get_level_target

func _init(new_name: String, new_stats: Dictionary, new_experience_points: int).(new_name, new_stats) -> void:
	type = "ally"
	experience_points = new_experience_points
	_recalculate_experience_points()

func total_experience_for_level(current_level: int):
	if 1 > current_level or current_level > 99:
		return null
	elif current_level == 1:
		return 0
	else:
		return int(floor(current_level * current_level * (current_level + 1) * (stats["level_rate"] / 256.0)))

func _recalculate_experience_points() -> void:
	while level_target < 99 and total_experience_for_level(level_target + 1) <= experience_points:
		level_target += 1

func level_up() -> Dictionary:
	assert(_get_pending_level_up(), "An ally can only level up when it has enough experience points")
	var increases := {}
	level += 1
	for stat in ["fight", "speed", "wisdom", "strength", "force"]:
		var constant: float = stats["level_up_stats"][stat]
		var increase := floor((constant + rand_range(0.0, 3.0)) / 2.0)
		increases[stat] = increase
		if stat == "fight":
			increases["offense"] = increase
		elif stat == "speed":
			increases["defense"] = increase
	for stat in increases:
		stats[stat] += increases[stat]

	var target_hp := min(stats.strength * 2 + 20, stats.strength + 255)
	var increase_range: int = int(floor(2 * (target_hp - stats.max_hp)))
	increase_range = int(floor(max(min(increase_range, 16), 2))) # n is always 2-16
	increases["max_hp"] = randi() % (increase_range - 1)

	var target_pp := floor(stats.force * 1.5)
	increase_range = int(floor(2 * (target_pp - stats.max_pp)))
	increase_range = int(floor(max(min(increase_range, 16), 2))) # n is always 2-16
	increases["max_pp"] = randi() % (increase_range - 1)

	stats.max_hp += increases["max_hp"]
	stats.max_pp += increases["max_pp"]
	return increases
