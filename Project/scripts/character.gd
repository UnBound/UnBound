# the base class for either an ally or an enemy

signal defeat(character)

var type: String

var stats: Dictionary
var name: String

func set_hp(new_value: int) -> void:
	if new_value <= 0:
		var is_new_defeat := hp > 0
		hp = 0
		if is_new_defeat:
			emit_signal("defeat", self)
	else:
		hp = new_value

func set_pp(new_value: int) -> void:
	if new_value <= 0:
		pp = 0
	else:
		pp = new_value

var hp: int setget set_hp
var pp: int setget set_pp

func rest() -> void:
	hp = stats["max_hp"]
	pp = stats["max_pp"]

func _init(new_name: String, new_stats: Dictionary) -> void:
	name = new_name
	stats = new_stats

	rest()

func attack_character(target):
	var damage: int
	var result: String
	if randf() <= (26.0 + (target.stats.fight - stats.fight) / 2.0) / 256.0:
		result = "miss"
	elif randf() <= (26.0 + (stats.fight - target.stats.fight) / 2.0) / 256.0:
		result = "smash"
	else:
		result = "hit"
	if result == "smash":
		damage = stats.offense
	elif stats.offense < target.stats.defense:
		damage = ((stats.offense * 3) - target.stats.defense) / 4
	else:
		damage = stats.offense - (target.stats.defense / 2)
	return [result, max(damage, 1)]

func attack(target):
	var attack_result = attack_character(target)
	return [0, attack_result[0], attack_result[1]]
