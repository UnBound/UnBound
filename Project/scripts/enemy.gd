extends "res://scripts/character.gd"

var description

func _init(name, stats).(name, stats):
	type = "enemy"
	#experience_points_drop = stats["experience_points_drop"]
	#money_drop = stats["money_drop"]
	#description = stats["description"]

func starting_attack_message():
	return name + " suddenly attacked!"

func attack(allies):
	var chosen_ally = randi() % allies.size()
	var attack_result = attack_character(allies[chosen_ally])
	return [chosen_ally, attack_result[0], attack_result[1]]
