extends Node2D

var ppu_tiles: Array
var metatiles: Array
var chunks: Array
var palettes: Array
var raw_palettes: Array
var sectors: Array
var main_map_rows: Array
var metatile_properties: Array
var metatile_properties_on_map: Array
var sprite_tiles: Array
var metatile_palettes: Array

# adding these makes the code much more readable
const SCREEN_SIZE_X = 256
const SCREEN_SIZE_Y = 224
const CHUNK_SIZE = 64
const ROW_SIZE = CHUNK_SIZE * 256 # 256 chunks per row
const SECTOR_SIZE = 256
const SECTORS_PER_ROW = ROW_SIZE / SECTOR_SIZE
const CHUNKS_PER_ROW = 256
const METATILES_PER_ROW = 1024
const METATILES_PER_SECTOR = 16

const NES_PPU_COLORS = [
	'#555555', '#001773', '#000786', '#2e0578', '#59024d', '#720011', '#6e0000', '#4c0800', # $00-07
	'#171b00', '#002a00', '#003100', '#002e08', '#002645', '#000000', '#000000', '#000000', # $08-0F
	'#a5a5a5', '#0057c6', '#223fe5', '#6e28d9', '#ae1aa6', '#d21759', '#d12107', '#a73700', # $10-17
	'#635100', '#186700', '#007200', '#007331', '#006a84', '#000000', '#000000', '#000000', # $18-1F
	'#feffff', '#2fa8ff', '#5d81ff', '#9c70ff', '#f772ff', '#ff77bd', '#ff7e75', '#ff8a2b', # $20-27
	'#cda000', '#81b802', '#3dc830', '#12cd7b', '#0dc5d0', '#3c3c3c', '#000000', '#000000', # $28-2F
	'#feffff', '#a4deff', '#b1c8ff', '#ccbeff', '#f4c2ff', '#ffc5ea', '#ffc7c9', '#ffcdaa', # $30-37
	'#efd696', '#d0e095', '#b3e7a5', '#9feac3', '#9ae8e6', '#afafaf', '#000000', '#000000'  # $38-3F
]

func reload_file():
	ppu_tiles = []
	metatiles = []
	chunks = []
	palettes = []
	main_map_rows = []
	metatile_properties = []
	metatile_properties_on_map = []
	metatile_palettes = []

	var file = File.new()
	file.open("res://gamedata/map.bin", File.READ)

	# header
	file.seek(5) # get past the "m1map" part
	var ppu_tile_count: int = file.get_16()
	assert(ppu_tile_count <= 0x2000, "ppu tile count is too large")
	var metatile_count: int = file.get_16()
	assert(metatile_count <= 0x2000, "metatile count is too large")
	var chunk_count: int = file.get_16()
	assert(chunk_count <= 0x2000, "chunk count is too large")
	var palette_count: int = file.get_16()
	assert(palette_count <= 0x800, "palette count is too large")
	var sector_count: int = file.get_16()
	assert(sector_count <= 0x2000, "sector count is too large")
	var main_map_item_count: int = file.get_16()
	var metatile_properties_count: int = file.get_16()
	assert(metatile_properties_count <= 0x2000, "metatile properties is too large")
	var metatile_palettes_count: int = file.get_16()
	assert(metatile_palettes_count <= 0x10000, "metatile palette count is too large")
	file.seek(32)

	var image : Array
	for ppu_tile_index in range(ppu_tile_count):
		image = []
		for index in range(64):
			image.append(file.get_8())
		ppu_tiles.append(image)

	for metatile_index in range(metatile_count):
		image = []
		for index in range(4):
			image.append(file.get_8())
		metatiles.append(image)

	for chunk_index in range(chunk_count):
		image = []
		for index in range(16):
			image.append(file.get_8())
		chunks.append(image)

	for palette_index in range(palette_count):
		image = []
		var raw_palette := []
		for index in range(4):
			var color: int = file.get_8()
			image.append(Color(NES_PPU_COLORS[color]))
			raw_palette.append(color)
		palettes.append(image)
		raw_palettes.append(raw_palette)

	for sector_index in range(sector_count):
		image = []
		for index in range(4):
			image.append(file.get_8())
		sectors.append(image)

	var main_map_item_index := 0
	while main_map_item_index < main_map_item_count:
		image = []
		for index in range(256):
			image.append(file.get_8())
			main_map_item_index += 1
		main_map_rows.append(image)

	for metatile_property in range(metatile_properties_count):
		metatile_properties.append(file.get_8())

	for metatile_palette in range(metatile_palettes_count):
		metatile_palettes.append(file.get_8())

func get_area(position: Vector2) -> int:
	var sector: int = (int(position.y / 64 / 4) * int(CHUNKS_PER_ROW / 4) + int(position.x / 64 / 4))
	return sectors[sector][0]

func _ready():
	reload_file()
