extends ColorRect

func _change_color():
	color = GameController.colors[0]

func _ready():
	GlobalSignal.add_listener("change_color", self, "_change_color")
	_change_color()
