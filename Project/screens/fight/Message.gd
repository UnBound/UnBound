extends "res://text_box.gd"

signal done

var active := false
var done := false
var _start_time: int
var _fast_time
var is_arrow_after := true
var _showing_you_won := false

var pending_text
var previous_text := ""
var queue := []


func _process(_delta) -> void:
	if done:
		return
	elif _showing_you_won:
		var gba_frames_elapsed := (OS.get_ticks_msec() - _start_time) / ((1.0 / 60.0) * 1000.0)
		if gba_frames_elapsed <= 12.0:
			$YouWon1.show()
			$YouWon2.hide()
		elif gba_frames_elapsed <= 18.0:
			$YouWon1.hide()
			$YouWon2.show()
		elif gba_frames_elapsed <= 24.0:
			$YouWon1.show()
			$YouWon2.hide()
		elif gba_frames_elapsed <= 30.0:
			$YouWon1.hide()
			$YouWon2.show()
		elif gba_frames_elapsed <= 48.0:
			$YouWon1.show()
			$YouWon2.hide()
		else:
			$YouWon1.show()
			$YouWon2.hide()
			done = true
			if is_arrow_after:
				$Arrow.start()
	else:
		$YouWon1.hide()
		var number_of_characters
		var speed = $"/root/Fight".base_speed / $"/root/Fight".speed
		if _fast_time == null:
			number_of_characters = int((OS.get_ticks_msec() - _start_time) / speed)
		else:
			number_of_characters = int((_fast_time - _start_time) / speed)
			number_of_characters += int((OS.get_ticks_msec() - _fast_time) / ($"/root/Fight".base_speed / 3))
		if number_of_characters > pending_text.length() + 2:
			done = true
			if is_arrow_after:
				$Arrow.start()
		else:
			# String.split returns a PoolStringArray, which doesn't support slicing,
			# so I need to explicitly convert it to an Array
			var lines := Array((previous_text + pending_text.substr(0, number_of_characters)).split("\n"))
			_set_content("\n".join(lines.slice(-2, -1)))

func _end_message() -> void:
	var next_message = queue.pop_front()
	if next_message == null:
		stop()
	elif next_message is int and next_message == 1:
		previous_text = ""
		pending_text = ""
		_end_message()
	else:
		show_message_without_clear(next_message)

func you_won() -> void:
	_set_content("")
	_start_time = OS.get_ticks_msec()
	done = false
	_showing_you_won = true

func go_fast() -> void:
	if active and _fast_time == null:
		_fast_time = OS.get_ticks_msec()

func show_message_without_clear(text, arrow_after := true) -> void:
	if previous_text.length() > 0 or pending_text.length() > 0:
		pending_text += "\n"
	previous_text += pending_text
	$Arrow.stop()
	show()
	is_arrow_after = arrow_after
	_start_time = OS.get_ticks_msec()
	done = false
	active = true
	_fast_time = null
	if text is int and text == 2:
		you_won()
	else:
		_showing_you_won = false
		pending_text = text

func show_message(text: String, arrow_after := true) -> void:
	previous_text = ""
	pending_text = ""
	show_message_without_clear(text, arrow_after)

func show_messages_without_clear(messages: Array, _arrow_after := true) -> void:
	queue = messages
	_end_message()

func show_messages(messages: Array, _arrow_after := true) -> void:
	previous_text = ""
	pending_text = ""
	show_messages_without_clear(messages, _arrow_after)

func stop() -> void:
	hide()
	active = false
	done = true
	pending_text = null
	queue = []
	_set_content("")
	$Arrow.stop()

func _input(event: InputEvent) -> void:
	if not active:
		return

	if event.is_action_pressed("ui_accept"):
		if done:
			if queue.size() == 0:
				emit_signal("done")
			else:
				_end_message()
		else:
			go_fast()

func _ready() -> void:
	stop()
	_label.align = Label.ALIGN_LEFT
