extends Control

signal end_battle

const Enemy = preload("res://screens/fight/Enemy.gd")

var speed := 2 # 1: slow, 2: medium, 3: fast
var base_speed := 60
var allies: Array
var enemies: Array
var _undefeated_allies := []
var _undefeated_enemies := []

var current_turn
var turns: Array
var player_actions := {}
var action
var pending_action

var attack_result
var target

var _character_meters: Array = []
var _defeat_queue: Array = []
var _experience_points: int = 0

var _character_moving = false

func _on_message_done():
	_character_moving = true
	var defeated = _defeat_queue.pop_back()
	if action == "end":
		for ally in allies:
			if ally.pending_level_up:
				#GameController.play_music(load("res://music/battle/Level Up.mp3"))
				var level_up_increases: Dictionary = ally.level_up()
				var messages := ["%s's level is now %d!" % [ally.name, ally.level]]
				var stat_names_capitialized := ["Fight", "Speed", "Wisdom", "Strength", "Force", "HP", "PP"]
				var index := 0
				for stat_increase in ["fight", "speed", "wisdom", "strength", "force", "max_hp", "max_pp"]:
					if level_up_increases[stat_increase] == 0:
						continue
					var message := "%s went up by %d." % [stat_names_capitialized[index], level_up_increases[stat_increase]]
					if index % 2 == 0:
						messages.append(message)
					else:
						messages[-1] += "\n" + message
					index += 1
				$Message.show_messages(messages)
		action = "stop"
	if action == "stop":
		$Message.stop()
		$AttackOptions.stop()
		GameController.stop_music()
		emit_signal("end_battle")
		return
	elif enemies.size() == 0:
		for ally in allies:
			if ally.hp > 0:
				ally.experience_points += _experience_points
		$Message.show_messages([2, 1, "%s earned %d experience point%s!" %
							   [allies[0].name, _experience_points, "" if _experience_points == 1 else "s"]])
		GameController.play_music(load("res://music/battle/You won!.ogg"))
		action = "end"
		return
	elif _undefeated_allies.size() == 0:
		GameController.play_music(load("res://music/Game over.mp3"))
		$Message.show_message("%s lost the battle..." % allies[0].name)
		for ally in allies:
			ally.rest()
		action = "stop"
		return
	elif defeated != null:
		if defeated.type == "enemy":
			_experience_points += defeated.stats.experience_points_drop
			$Message.show_message("%s was defeated!" % defeated.name)
			allies.erase(defeated)
			_undefeated_enemies.erase(defeated)
		elif defeated.type == "ally":
			_undefeated_allies.erase(defeated)
			_update_meters()
			$Message.show_message("%s got hurt and collapsed..." % defeated.name)
		return
	if current_turn > turns.size() - 1:
		new_round()
	var attacker = turns[current_turn]
	if attacker.type == "ally" and not attacker in _undefeated_allies:
		current_turn += 1
		_on_message_done()
		return
	#var attack_result
	#var target
	if action == "askplayer":
		$Message.stop()
		_next_player_choice()
	elif action == "startturn":
		if attacker.type == "enemy":
			attack_result = attacker.attack(_undefeated_allies)
			target = _undefeated_allies[attack_result[0]]
			$Message.show_message("%s attacks!" % attacker.name)
			action = "attack"
		elif attacker.type == "ally":
			var advance_turn: bool = true
			var chosen_action: String = player_actions[attacker][0]
			match chosen_action:
				"attack":
					attack_result = attacker.attack(enemies[0])
					target = enemies[0]
					$Message.show_message("%s attacks!" % attacker.name)
					action = "attack"
					advance_turn = false
				"guard":
					$Message.show_message("%s is guarding." % attacker.name)
				"check":
					$Message.show_messages([
						"%s checked the %s!" % [attacker.name, enemies[0].name],
						"Offense is %d.\nDefense is %d." % [enemies[0].stats.offense, enemies[0].stats.defense],
					] + enemies[0].stats.description)
				"run":
					if GameController.battle_type == GameController.BATTLE_TYPE_BOSS or randf() <= 0.5:
						$Message.show_messages(["%s ran away!" % attacker.name, "Couldn't run!"])
					else:
						$Message.show_messages(["%s ran away!" % attacker.name, "Ran away successfully!"])
						action = "stop"
			if advance_turn:
				current_turn += 1
	elif action == "attack":
		if attack_result[1] == "miss":
			$Message.show_message("%s dodged quickly!" % target.name)
		else:
			var damage = attack_result[2]
			if player_actions.get(target, [null, null])[0] == "guard":
				damage = ceil(damage / 2.0)
			target.hp -= damage
			_update_meters()
			if attack_result[1] == "smash":
				$Message.show_message("SMAAAASH!\n%s took %d damage!" % [target.name, damage])
			else:
				$Message.show_message("%s took %d damage!" % [target.name, damage])
		action = "startturn"
		current_turn += 1

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("b") and action == "askplayer" and\
	   $Message.done and player_actions.size() > 0:
		var _useless = player_actions.erase(_undefeated_allies[player_actions.size() - 2])
		_next_player_choice()

func new_round():
	current_turn = 0
	player_actions.clear()
	turns = _get_turns(_undefeated_allies, _undefeated_enemies)
	action = "askplayer"

func character_defeat(character) -> void:
	_defeat_queue.append(character)

func _next_player_choice() -> void:
	_update_meters()
	if action == "chooseenemy":
		$AttackOptions.stop()
	elif player_actions.size() >= _undefeated_allies.size():
		$AttackOptions.stop()
		action = "startturn"
		_on_message_done()
	else:
		var options = ["attack", "goods", "ability", "psi", "guard", "auto", "run"]
		if GameController.battle_type == GameController.BATTLE_TYPE_BATTLE_MEMORY:
			options.erase("run")
			options.append("exit")
		$AttackOptions.show_options(options)

func _on_enemy_choose(enemy: int) -> void:
	var current_ally = _undefeated_allies[player_actions.size() - 1]
	if action == "chooseenemy":
		player_actions[current_ally] = [pending_action, enemy]
		pending_action = null
		action = "askplayer"
		_next_player_choice()

func _on_option_choose(option: String) -> void:
	var current_ally = _undefeated_allies[player_actions.size() - 1]
	if option == "exit":
		action = "stop"
		_on_message_done()
		return
	elif option == "attack":
		pending_action = "attack"
		action = "chooseenemy"
	elif option in ["guard", "run"]:
		player_actions[current_ally] = [option, null]
	elif option == "ability":
		player_actions[current_ally] = ["check", null]
	_next_player_choice()

func _update_meters() -> void:
	var index = 0
	for ally in allies:
		var meter = _character_meters[index]
		#meter.hp = ally.hp
		#meter.pp = ally.pp
		meter.defeated = not ally in _undefeated_allies
		meter.is_turn = action == "askplayer" and $Message.done and player_actions.size() == _undefeated_allies.find(ally)
		index += 1

func _redraw_meters() -> void:
	$"/root/Fight/CharacterIndicators".clear_meters()
	_character_meters.clear()
	for ally in allies:
		_character_meters.append($"/root/Fight/CharacterIndicators".create_meter(ally.name))
	_update_meters()

func _ready() -> void:
	GlobalSignal.add_emitter("end_battle", self)

	GameController.play_music(load("res://music/enemies/Battle with a Dangerous Foe.mp3"))

	current_turn = 0
	allies = GameController.allies
	enemies = GameController.enemies
	_undefeated_allies.clear()
	for ally in allies:
		if ally.hp > 0:
			_undefeated_allies.append(ally)
	var enemy_index := 0
	_undefeated_enemies.clear()
	for enemy in enemies:
		var enemy_scene = Enemy.new(enemy, enemy_index)
		enemy_scene.connect("choose", self, "_on_enemy_choose")
		$Enemies.add_child(enemy_scene)
		enemy_index += 1
	for character in enemies + allies:
		character.connect("defeat", self, "character_defeat")
	_redraw_meters()
	_experience_points = 0
	new_round()
	$Message.show_message(enemies[0].starting_attack_message())

func compare_turns(a, b) -> bool:
	if a[1] != b[1]:
		return a[1] > b[1]
	else:
		# if there's a tie, enemies act before allies, and later characters act first
		# this as easy as comparing indexes due to everything being sorted already
		# as for enemy order, it doesn't specify and it's so rare that it doesn't really matter
		return a[2] > b[2]

func _get_turns(turn_allies: Array, turn_enemies: Array) -> Array:
	# I need the equation for this figured out for MOTHER 1!
	# the only thing I could find was the vague notion that higher speed helps
	# currently I'm using the equation for MOTHER 2, which works well enough (maybe it's the same?) for this game
	var order = []
	var character_index = 0
	for character in turn_enemies + turn_allies:
		var modified_speed = character.stats.speed + rand_range(0.5, 1.5)
		order.append([character, modified_speed, character_index])
		character_index += 1
	order.sort_custom(self, "compare_turns")
	for index in range(order.size()):
		order[index] = order[index][0]
	return order
