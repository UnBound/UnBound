extends CenterContainer

var _all_meters = []

const Indicator = preload("res://screens/fight/Indicator.tscn")

func create_meter(new_name: String) -> TextureRect:
	var attack_meter: TextureRect = Indicator.instance()
	attack_meter.character_name = new_name
	$Meters.add_child(attack_meter)
	return attack_meter

func clear_meters() -> void:
	while _all_meters.size() > 0:
		_all_meters.pop_back().queue_free()
