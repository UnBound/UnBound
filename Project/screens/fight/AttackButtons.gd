extends HBoxContainer

signal choose(option)
signal hover
signal change_description(description)

func _on_option_choose(option):
	emit_signal("choose", option)

func _on_option_hover():
	emit_signal("hover")

func _on_option_leave(_option):
	emit_signal("change_description", "")

func show_options(options):
	for name in GameController.option_names:
		var node = get_node(name)
		if name in options:
			node.show()
		else:
			node.hide()

func _new_option(name):
	var button = TextureButton.new()
	button.set_name(name)
	button.expand = true
	button.stretch_mode = button.STRETCH_KEEP_CENTERED
	button.texture_normal = load("res://assets/attackbuttons/%sunselected.png" % name)
	button.texture_hover = load("res://assets/attackbuttons/%sselected.png" % name)
	button.texture_pressed = load("res://assets/attackbuttons/%spressed.png" % name)
	button.margin_left = 16
	button.margin_bottom = 16
	button.rect_position = Vector2(16, 16)
	button.rect_size = Vector2(16, 16)
	button.rect_min_size = Vector2(16, 16)
	button.set_script(load("res://screens/fight/Option.gd"))
	button.connect("pressed", button, "_on_pressed")
	button.connect("draw", self, "_on_option_hover")
	button.connect("choose", self, "_on_option_choose")
	add_child(button)

func _ready():
	for option in GameController.option_names:
		_new_option(option)
