extends TextureButton

signal choose(option)
signal hover(option)
signal leave(option)

func _on_pressed():
	emit_signal("choose", name)

func _on_hover():
	emit_signal("hover", name)

func _on_leave():
	emit_signal("leave", name)
