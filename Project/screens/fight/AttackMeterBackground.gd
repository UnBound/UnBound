extends TextureRect

const _base_image: Image = preload("res://textures/attackindicator.png")
var _image: Image
var _texture: ImageTexture

func _update_defeated(new_defeated: bool):
	_defeated = new_defeated
	_change_color()

var _defeated: bool

func _change_color() -> void:
	_texture = ImageTexture.new()
	texture = _texture
	_image = Image.new()
	_image.copy_from(_base_image)
	var colors: Array
	if _defeated:
		colors = [Color("#945a5a"), Color("#ad6b63")]
	else:
		colors = GameController.colors
	GameController.replace_color(_image, Color("#ffffff"), colors[0])
	GameController.replace_color(_image, Color("#000000"), colors[1])
	_texture.create_from_image(_image, Texture.FLAG_REPEAT)

func _ready() -> void:
	GlobalSignal.add_listener("change_color", self, "_change_color")
	_change_color()
