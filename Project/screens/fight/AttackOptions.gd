extends Node2D

func _ready() -> void:
	hide()

func show_options(options) -> void:
	$Buttons.show_options(options)
	_on_hover()
	show()

func stop() -> void:
	hide()

func _on_hover() -> void:
	for button in $Buttons.get_children():
		if button.is_hovered():
			set_description(GameController.option_names[button.name])
			return
	set_description("")

func set_description(description: String) -> void:
	$Description.content = description
