extends TextureButton

signal choose(index)

var enemy
var index: int

func _init(current_enemy, current_index: int):
	enemy = current_enemy
	index = current_index

func _ready():
	if enemy.name == "Lamp":
		texture_normal = load("res://sprites/enemies/doll.png")
	elif enemy.name == "Doll":
		texture_normal = load("res://sprites/enemies/doll.png")
	else:
		texture_normal = load("res://sprites/enemies/doll.png")

func _pressed():
	emit_signal("choose", index)
