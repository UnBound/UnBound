extends TextureRect

var start_time
var active = false
var previous_tick
var initial_position = rect_position[1]

func start():
	if active:
		rect_position[1] = initial_position
	previous_tick = null
	start_time = OS.get_ticks_msec()
	active = true
	show()

func stop():
	hide()
	rect_position[1] = initial_position
	active = false

func _input(event):
	if event is InputEventKey and event.scancode == KEY_1:
		if not active:
			start()
	elif event is InputEventKey and event.scancode == KEY_2:
		stop()

func _ready():
	stop()

func _process(_delta):
	if not active:
		return
	# MOTHER 3 has the arrow start at the top for 300 ms and then never
	# again stay in the same position for more then 150 ms.
	var ticks = int((OS.get_ticks_msec() - start_time) / 150)
	if previous_tick == ticks:
		return
	else:
		previous_tick = ticks
	if ticks == 0:
		# when appearing, the arrow stays still for 300 ms (as opposed to 150)
		return
	elif (ticks - 1) % 10 < 5:
		rect_position[1] += 1
	else:
		rect_position[1] -= 1
	
