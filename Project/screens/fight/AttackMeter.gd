extends TextureRect

const CharacterSprite: PackedScene = preload("res://screens/CharacterSprite.tscn")
const _base_image: Image = preload("res://containers/attackindicator.png")

signal update_defeated(defeated)

func _set_hp(new_hp: int) -> void:
	hp = new_hp
	_update_values()

func _get_hp() -> int:
	return hp

var hp: int setget _set_hp, _get_hp

func _set_pp(new_pp: int) -> void:
	pp = new_pp
	_update_values()

func _get_pp() -> int:
	return pp

var pp: int setget _set_pp, _get_pp

var is_turn := false

const _one := -8
const _two := 7

var _grid: HBoxContainer
var _sprite: AnimatedSprite
var _real_sprite_position := Vector2(0, _two)
var _digit_rects := [[], []]
var _texture: ImageTexture
var _image: Image

func _change_color() -> void:
	_texture = ImageTexture.new()
	texture = _texture
	_image = Image.new()
	_image.copy_from(_base_image)
	if defeated:
		GameController.replace_color(_image, Color("#ffffff"), Color("#bd7b7b"))
	_texture.create_from_image(_image, Texture.FLAG_REPEAT)

func _update_values() -> void:
	var index: int = 0
	for value in [hp, pp]:
		while _digit_rects[index].size() > 0:
			_digit_rects[index].pop_back().queue_free()
		var _text = "%3d" % value
		for _digit in range(3):
			var _digit_rect = ColorRect.new()
			_digit_rect.color = Color("#bd7b7b") if defeated else Color("#ffffff")
			var padding
			if _text[_digit] == " ":
				padding = 0
			else:
				padding = 2
			_digit_rect.rect_size = Vector2(7, 7 - padding * 2)
			_digit_rect.rect_position = Vector2(29 + 8 * _digit, 13 + index * 10 + padding)
			_digit_rect.show_behind_parent = true
			_digit_rects[index].append(_digit_rect)
			add_child(_digit_rect)
		#get_node("Text%d" % index).text = _text # todo
		index += 1

func _get_defeated() -> bool:
	return defeated

func _set_defeated(new_defeated: bool) -> void:
	defeated = new_defeated
	_change_color()
	_update_values()
	emit_signal("update_defeated", defeated)

var defeated: bool setget _set_defeated, _get_defeated

func _set_character_name(new_character_name: String) -> void:
	character_name = new_character_name
	$"NameContainer/Name".text = character_name
	_make_sprite()

func _get_character_name() -> String:
	return character_name

var character_name: String setget _set_character_name, _get_character_name

func _make_sprite() -> void:
	if not character_name in ["Ken", "Ana"]:
		return
	var sprite: AnimatedSprite = CharacterSprite.instance()
	sprite.character_name = character_name
	if _sprite != null:
		_sprite.queue_free()
	$"SpriteContainer/SpriteGrid".add_child(sprite)
	_sprite = sprite

func set_name(new_name: String) -> void:
	_set_character_name(new_name)

func _ready() -> void:
	hp = 0
	pp = 0
	_update_values()
	_change_color()
	_make_sprite()

func _process(delta) -> void:
	if _sprite == null:
		return
	var speed := 50 # 1 / 20 of a second for complete movement
	_real_sprite_position = _real_sprite_position.move_toward(Vector2(0, _one if is_turn else _two), delta * speed)
	_sprite.position[1] = round(_real_sprite_position[1])
