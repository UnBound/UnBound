extends TextureRect

func _ready() -> void:
	var background = ColorRect.new()
	background.rect_position = Vector2(3, 3)
	background.rect_size = rect_size - Vector2(6, 6)
	background.show_behind_parent = true
	background.color = Color("#000000")
	background.set_script(load("res://colored_background.gd"))
	add_child(background)
