extends VBoxContainer

const MENU_ITEM_SCENE = preload("res://screens/battlememory/MenuItem.tscn")

const SIZE := 8
var _scrolled := 0
var _enemies := []

func _set_selected_enemy(new_selected_enemy: int, scroll_with := true) -> void:
	selected_enemy = int(max(min(new_selected_enemy, GameController.enemy_stats.size() - 1), 0))
	var selected_enemy_index: int = selected_enemy - _scrolled
	while true:
		selected_enemy_index = selected_enemy - _scrolled
		if selected_enemy_index < 0:
			if scroll_with:
				_scrolled = selected_enemy
				_redraw()
			else:
				selected_enemy = _scrolled
		elif selected_enemy_index > SIZE - 1:
			if scroll_with:
				_scrolled = selected_enemy - (SIZE - 1)
				_redraw()
			else:
				selected_enemy = _scrolled + (SIZE - 1)
		else:
			break
	var current_enemy_number := 0
	for enemy in _enemies:
		enemy.selected = current_enemy_number == selected_enemy_index
		current_enemy_number += 1
func _get_selected_enemy() -> int:
	return selected_enemy
var selected_enemy := 0 setget _set_selected_enemy, _get_selected_enemy

func _on_select_enemy(enemy_number: int) -> void:
	GameController.enemies = [GameController.get_enemy(_enemies[enemy_number].content)]
	GameController.battle_type = GameController.BATTLE_TYPE_BATTLE_MEMORY
	GameController.fight()

func _on_hover_enemy(enemy_number: int) -> void:
	_set_selected_enemy(_scrolled + enemy_number)

func _redraw() -> void:
	var index := 0
	for enemy in GameController.enemy_stats.slice(_scrolled, _scrolled + SIZE - 1):
		_enemies[index].content = enemy["name"]
		_enemies[index].visible = true
		index += 1
	while index < SIZE:
		_enemies[index].visible = false
		index += 1
	$"../ArrowUp".visible = _scrolled > 0
	$"../ArrowDown".visible = _scrolled + SIZE + 1 <= GameController.enemy_stats.size()

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_down"):
		 _set_selected_enemy(_get_selected_enemy() + 1)
	if event.is_action_pressed("ui_up"):
		 _set_selected_enemy(_get_selected_enemy() - 1)
	if event.is_action_pressed("ui_right"):
		var selected_enemy_index = _get_selected_enemy() - _scrolled
		if _scrolled >= GameController.enemy_stats.size() - SIZE:
			_set_selected_enemy(_scrolled + SIZE)
		else:
			_scrolled += SIZE
			if _scrolled > GameController.enemy_stats.size() - SIZE:
				_scrolled = GameController.enemy_stats.size() - SIZE
			_set_selected_enemy(_scrolled + selected_enemy_index)
		_redraw()
		#_set_selected_enemy(_scrolled + (SIZE - 1))
	if event.is_action_pressed("ui_left"):
		var selected_enemy_index = _get_selected_enemy() - _scrolled
		if _scrolled <= 0:
			_set_selected_enemy(_scrolled)
		else:
			_scrolled -= SIZE
			if _scrolled < 0:
				_scrolled = 0
			_set_selected_enemy(_scrolled + selected_enemy_index)
		_redraw()
	if event.is_action_pressed("start"):
		_on_select_enemy(selected_enemy - _scrolled)

func _ready() -> void:
	for enemy_number in range(8):
		var menu_item = MENU_ITEM_SCENE.instance()
		_enemies.append(menu_item)
		menu_item.connect("select_enemy", self, "_on_select_enemy", [enemy_number])
		menu_item.connect("mouse_entered", self, "_on_hover_enemy", [enemy_number])
		add_child(menu_item)
	_set_selected_enemy(0)
	_redraw()

func _on_arrow_up_pressed() -> void:
	_scrolled -= 1
	_set_selected_enemy(_get_selected_enemy(), false)
	_redraw()

func _on_arrow_down_pressed() -> void:
	_scrolled += 1
	_set_selected_enemy(_get_selected_enemy(), false)
	_redraw()
