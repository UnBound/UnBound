extends Node2D

signal exit

func _on_level_up_pressed() -> void:
	var ally = GameController.allies[0]
	ally.level += 1
	_update_level()
func _on_level_up_10_pressed() -> void:
	var ally = GameController.allies[0]
	ally.level += 10
	_update_level()

func _input(event: InputEvent):
	if event.is_action_pressed("b"):
		emit_signal("exit")

func _update_level() -> void:
	var ally = GameController.allies[0]
	$Level.content = "Level %d" % ally.level

func _ready() -> void:
	$Description.content = "Battle Memory"
	_update_level()
