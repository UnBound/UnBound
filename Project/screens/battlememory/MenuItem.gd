extends TextureButton

signal select_enemy

func _set_content(new_content: String):
	$Text.text = new_content
func _get_content() -> String:
	return $Text.text
var content: String setget _set_content, _get_content

func _update_selected() -> void:
	$Selection.visible = selected

func _set_selected(is_selected: bool) -> void:
	selected = is_selected
	_update_selected()
func _get_selected() -> bool:
	return selected
var selected: bool = false setget _set_selected, _get_selected

func _ready() -> void:
	_update_selected()

func _pressed() -> void:
	emit_signal("select_enemy")
