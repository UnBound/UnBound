extends WindowDialog

signal selected(path)

func _on_button_pressed():
	print("selected ", $Input.text)
	emit_signal("selected", $Input.text)
