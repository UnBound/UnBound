extends HBoxContainer

const MenuBarFont = preload("res://screens/overworld/MenuBarFont.tres")

signal go_to

func make_shortcut(scancode: int, modifiers := []) -> ShortCut:
	var new_shortcut = ShortCut.new()
	new_shortcut.shortcut = InputEventKey.new()
	new_shortcut.shortcut.scancode = scancode
	new_shortcut.shortcut.control = modifiers.has("ctrl")
	new_shortcut.shortcut.alt = modifiers.has("alt")
	new_shortcut.shortcut.shift = modifiers.has("shift")
	new_shortcut.shortcut.meta = modifiers.has("meta")
	return new_shortcut

func make_menu_button(name: String, shortcut = null, function = null, function_target = self) -> MenuButton:
	var menu_button := MenuButton.new()
	menu_button.enabled_focus_mode = Control.FOCUS_NONE # so there isn't a blue outline around the button after clicking
	menu_button.text = name
	if shortcut != null:
		menu_button.shortcut = shortcut
	if function != null:
		menu_button.get_popup().connect("id_pressed", function_target, function)
	menu_button.add_font_override("font", MenuBarFont) # fixes the font size being too small
	menu_button.get_popup().add_font_override("font", MenuBarFont)
	return menu_button

func make_menu(name: String, shortcut = null, function = null, function_target = self) -> PopupMenu:
	var menu_button := make_menu_button(name, shortcut, function, function_target)
	add_child(menu_button)
	return menu_button.get_popup()

func file_option_press(id: int) -> void:
	if id == 0:
		ROMController.save()
	elif id == 2:
		print("Bye")
		get_tree().quit()

func game_option_press(id: int) -> void:
	if id == 0:
		emit_signal("go_to")

func _ready() -> void:
	var file := MenuButton.new()
	file.enabled_focus_mode = Control.FOCUS_NONE
	var file_popup := make_menu("File", make_shortcut(KEY_F, ["alt"]), "file_option_press")
	var game_popup := make_menu("Game", make_shortcut(KEY_G, ["alt"]), "game_option_press")

	file_popup.add_item("Save", 0)
	file_popup.set_item_shortcut(0, make_shortcut(KEY_S, ["ctrl"]))
	file_popup.add_separator("", 1)
	file_popup.add_item("Exit", 2)
	file_popup.set_item_shortcut(2, make_shortcut(KEY_Q, ["ctrl"]))

	game_popup.add_item("Go to...", 0)
	game_popup.set_item_shortcut(0, make_shortcut(KEY_G, ["ctrl"]))
