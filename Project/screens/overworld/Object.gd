extends Node2D

signal edit(node, properties)
signal trigger(node, properties)

const PRESENT = 0x20

var door_like: bool

var properties: Dictionary setget _set_properties, _get_properties

func _update_properties(edited := true) -> void:
	if properties["type"] <= 4:
		door_like = true
	else:
		door_like = false
	global_position = Vector2(properties.position_x, properties.position_y)
	if "sprite" in properties:
		$Sprite.visible = true
		$Sprite.sprite = properties["sprite"]
		$Sprite.direction = properties["direction"]
	else:
		$Sprite.visible = false
	if GameController.debug_mode:
		$DebugView.visible = true
		if edited:
			$DebugView.update_properties(properties)
		else:
			$DebugView.redraw(properties)
	#if properties.type == PRESENT:
	#	$PresentImage.visible = true
	#else:
	#	$PresentImage.visible = false
func _set_properties(new_properties: Dictionary) -> void:
	properties = new_properties
	_update_properties()
func _get_properties() -> Dictionary:
	return properties
func set_properties_without_reload(new_properties: Dictionary) -> void:
	properties = new_properties
	_update_properties(false)

func init(new_properties: Dictionary) -> void:
	properties = new_properties
	_update_properties(false)

# called when the object editor edits a property
func update_properties(new_properties: Dictionary) -> void:
	properties = new_properties
	_update_properties()

func _on_click() -> void:
	emit_signal("edit", self, properties)

func _on_trigger() -> void:
	emit_signal("trigger", self, properties)
