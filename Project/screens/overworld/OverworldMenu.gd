extends Control

# todo: finish making the menu

signal select(option)

func _set_opened(value: bool) -> void:
	opened = value
	visible = opened
func _get_opened() -> bool:
	return opened
var opened: bool setget _set_opened, _get_opened

func open() -> void:
	_set_opened(true)

func close() -> void:
	_set_opened(false)

func _on_talk_pressed() -> void:
	close()
	emit_signal("select", "talk")

func _on_check_pressed() -> void:
	close()
	emit_signal("select", "check")

func _on_goods_pressed() -> void:
	close()
	emit_signal("select", "goods")

func _on_psi_pressed() -> void:
	close()
	emit_signal("select", "psi")
