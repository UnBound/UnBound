extends Node2D

var animated_sprite: AnimatedSprite
var ready := false

func _set_sprite(new_sprite: int) -> void:
	sprite = new_sprite
	if ready:
		_update_sprite()
var sprite: int setget _set_sprite
func _set_direction(new_direction: int) -> void:
	direction = new_direction
	if ready:
		_update_sprite()
var direction := 0 setget _set_direction
func _set_animation(new_animation: int) -> void:
	animation = new_animation
	if ready:
		_update_sprite()
var animation := 0 setget _set_animation

func _ready() -> void:
	animated_sprite = AnimatedSprite.new()
	animated_sprite.frames = SpriteFrames.new()
	add_child(animated_sprite)

	_update_sprite()
	ready = true

func _update_sprite() -> void:
	animated_sprite.frames.clear("default")
	animated_sprite.frames.add_frame("default", ROMController.get_sprite(sprite + direction + animation))
