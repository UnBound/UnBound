extends Node2D

var speed := 4.0

var x := 0.0
var y := 0.0

var current_area: int

# adding these makes the code much more readable
const SCREEN_SIZE_X = 256
const SCREEN_SIZE_Y = 224
const CHUNK_SIZE = 64
const ROW_SIZE = CHUNK_SIZE * 256 # 256 chunks per row
const SECTOR_SIZE = 256
const SECTORS_PER_ROW = ROW_SIZE / SECTOR_SIZE
const CHUNKS_PER_ROW = 256
const METATILES_PER_ROW = 1024
const METATILES_PER_SECTOR = 16

const MOVEMENT_UNIT = 16 # todo: allow the user to set this and enable pixel movement

const ObjectScene = preload("res://screens/overworld/Object.tscn")
const ObjectEdit = preload("res://screens/overworld/debug/ObjectEditor.tscn")

func _ready() -> void:
	print("chunks")
	_update_chunks()
	print("chunks!")
	x = 35 * MOVEMENT_UNIT
	y = 2 * MOVEMENT_UNIT
	$ViewportContainer/Viewport/Ken.move_to_without_emit(x, y)
	_update_position()
	print("CHUNKS")
	_update_chunks()
	print("CHUNKS!")
	_update_objects()
	# initial position
	# todo: save the position to a file when exiting and load it when it opens again
	#_update_position()
	#_update_chunks()

func _update_chunks() -> void:
	var chunks := []
	var area: int = MapController.get_area($ViewportContainer/Viewport/Ken.global_position)
	var camera_x: int = ($ViewportContainer/Viewport/Camera.position.x - (SCREEN_SIZE_X / 2)) / CHUNK_SIZE
	var camera_y: int = ($ViewportContainer/Viewport/Camera.position.y - 80) / CHUNK_SIZE
	for x in range(camera_x, camera_x + 5):
		for y in range(camera_y, camera_y + 4):
			chunks.append([x, y])
	$ViewportContainer/Viewport/Sector.draw_chunks(chunks, area)

func _update_objects() -> void:
	for object in GameController.objects:
		var sector: int = int(object.position_y / SECTOR_SIZE) * int(SECTORS_PER_ROW) + int(object.position_x / SECTOR_SIZE)
		var area: int = MapController.sectors[sector][0]
		if object.area == area:
			var new_object = ObjectScene.instance()
			new_object.set_properties_without_reload(object)
			new_object.connect("edit", self, "_on_object_edit")
			new_object.connect("trigger", self, "_on_object_trigger")
			$ViewportContainer/Viewport/Objects.add_child(new_object)

func _on_object_trigger(node: Node2D, properties: Dictionary) -> void:
	print({"position_x": properties.position_x, "position_y": properties.position_y})
	print({"real_position_x": node.position.x, "real_position_y": node.position.y})
	if "target_x" in properties and "target_y" in properties:
		$ViewportContainer/Viewport/Ken.move_to(properties["target_x"],
												properties["target_y"])
		_update_position()
		_update_chunks()

func _on_object_edit(node: Node, properties: Dictionary) -> void:
	var editor: Control = $Debug/ObjectEditor
	editor.load_object(properties, node)

const MOVE_CAMERA_WITH_PLAYER = true
func _update_position() -> void:
	if MOVE_CAMERA_WITH_PLAYER:
		$ViewportContainer/Viewport/Camera.position = Vector2(x + 120 - 60, y + 80 - 40)
	$ViewportContainer/Viewport/Ken.position = Vector2(x, y)
	$Popups/GoTo.set_value(int(x / MOVEMENT_UNIT), int(y / MOVEMENT_UNIT))

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("start"):
		var position = $ViewportContainer/Viewport/Ken.position
		#print(position)
		var sector: int = (int(position.y / 64 / 4) * int(CHUNKS_PER_ROW / 4) + int(position.x / 64 / 4))
		print("sector ", sector, " (area ", MapController.sectors[sector][0], ")")

	if false and event is InputEventMouseButton and event.is_pressed(): # todo: remove debug code
		var position: Vector2 = get_local_mouse_position()
		var metatile_position = [int(position.x / 16), int(position.y / 16)]
		var metatile: int = $ViewportContainer/Viewport/Sector.coordinate_metatile_properties[metatile_position]
		var properties: int = MapController.metatile_properties[metatile]
		#print("--")
		#print("position", [int(position.x / 16), int(position.y / 16)])
		#print(metatile)
		#print("%x" % properties)

#func _process(_delta: float) -> void:
	#var updated := false
	#if Input.is_action_pressed("ui_up"):
	#	y -= speed
	#	updated = true
	#if Input.is_action_pressed("ui_down"):
	#	y += speed
	#	updated = true
	#if Input.is_action_pressed("ui_left"):
	#	x -= speed
	#	updated = true
	#if Input.is_action_pressed("ui_right"):
	#	x += speed
	#	updated = true
	#if updated:
	#	_update_position()
	#	_update_chunks()
	#if Input.is_action_just_pressed("a"):
	#	$UI/OverworldMenu.open()
	#if Input.is_action_just_pressed("b"):
	#	$UI/OverworldMenu.close()
	#if Input.is_action_just_pressed("l"):
	#	$ViewportContainer/Viewport/Camera.zoom = Vector2(1, 1)
	#	$ViewportContainer/Viewport/Camera.get_child("ColorRect").visible = false
	#elif Input.is_action_just_pressed("r"):
	#	$ViewportContainer/Viewport/Camera.zoom = Vector2(2, 2)
	#	$ViewportContainer/Viewport/Camera.get_child("ColorRect").visible = true

func _on_move(new_x: float, new_y: float) -> void:
	x = new_x
	y = new_y
	_update_position()
	_update_chunks()

func _on_menu_select(option) -> void:
	# obviously just placeholders before working on evaluating the script
	if option == "talk":
		print("Who are you talking to?")
	if option == "check":
		print("No problem.")

func _on_object_editor_update_properties(node: Node, properties: Dictionary) -> void:
	node.update_properties(properties)

func _go_to() -> void:
	$Popups/GoTo.set_value(int(x / MOVEMENT_UNIT), int(y / MOVEMENT_UNIT)) # this is redundent because this is used in _update_position, but in case there's some wierd case where it doesn't automatically call this, it'll help
	$Popups/GoTo.popup()

func _on_go_to_changed(new_x: int, new_y: int) -> void:
	print(new_x * MOVEMENT_UNIT, ", ", new_y * MOVEMENT_UNIT)
	$ViewportContainer/Viewport/Ken.move_to(new_x * MOVEMENT_UNIT, new_y * MOVEMENT_UNIT)
	print("changed")
	# no need to update anything, it'll emit a signal when it moves
