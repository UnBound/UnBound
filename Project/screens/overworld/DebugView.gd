extends Control

signal click
signal trigger

# define the directions from MOTHER 1's ROM
# the direction constants makes it easier to read (e.g. [UP, LEFT] instead of [1, -1])
const NONE = 0
const UP = 1
const DOWN = -1
const RIGHT = 1
const LEFT = -1
# this makes the indexing easier to read (e.g. direction[X] instead of direction[1])
# the reason they're backwards is because it makes the directions' language correct (up-right instead of right-up),
# and it doesn't change anything because of them being constants
const X = 1
const Y = 0
const DIRECTIONS = [
	[UP, NONE],
	[UP, RIGHT],
	[NONE, RIGHT],
	[DOWN, RIGHT],
	[DOWN, NONE],
	[DOWN, LEFT],
	[NONE, LEFT],
	[UP, LEFT],
	[NONE, NONE] # shuffle in place, only used for teleporting
]

# type constants
const HOLE = 0x4
const PRESENT = 0x20

var _transparency: float
var _old_transparency: float
var _properties: Dictionary

var _ready := false
var _pending_redraw := false

func _update_transparency() -> void:
	if not _ready:
		return
	$Background.color.a = _transparency

func _ready() -> void:
	_old_transparency = $Background.color.a # get it from the Godot editor
	_transparency = _old_transparency
	_update_transparency()
	_ready = true
	if _pending_redraw:
		_pending_redraw = false
		redraw(_properties)

func redraw(properties: Dictionary) -> void:
	"redraws the DebugView without queuing writing the properties to the ROM"
	_properties = properties
	if not _ready:
		_pending_redraw = true
		return
	var direction: Array = DIRECTIONS[properties.direction]
	$Label.text = str(properties.type)
	if properties.type <= 4: # door-like
		$Background.color = Color.yellow
		if properties.type == HOLE:
			# holes can be entered on all sides
			$TopDirection.visible = true
			$BottomDirection.visible = true
			$LeftDirection.visible = true
			$RightDirection.visible = true
		else:
			# shows the directions where you can enter the door, as opposed to where you can't,
			# so the directions are inversed
			$TopDirection.visible = direction[Y] == DOWN
			$BottomDirection.visible = direction[Y] == UP
			$LeftDirection.visible = direction[X] == RIGHT
			$RightDirection.visible = direction[X] == LEFT
	else:
		$TopDirection.visible = false
		$BottomDirection.visible = false
		$LeftDirection.visible = false
		$RightDirection.visible = false
		$Background.color = Color.gray
	_update_transparency()
	if "sprite" in properties:
		$Background.visible = false # otherwise there would be a grey background around all sprites
	else:
		$Background.visible = true

func update_properties(properties: Dictionary) -> void:
	"redraws the DebugView from the new object properties and queues it for writing to the ROM"
	redraw(properties)
	ROMController.edit_object(properties)

func _on_mouse_entered():
	_transparency = 0.2
	_update_transparency()

func _on_mouse_exited():
	_transparency = _old_transparency
	_update_transparency()

func _on_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.is_pressed():
		if event.button_index == BUTTON_LEFT:
			emit_signal("click")
		if event.button_index == BUTTON_RIGHT:
			emit_signal("trigger")
