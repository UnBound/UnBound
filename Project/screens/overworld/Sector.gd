extends Node2D

const Chunk := preload("res://screens/overworld/Chunk.tscn")

var main_map_rows := MapController.main_map_rows
var sectors := MapController.sectors
var global_tileset: TileSet
var global_tileset_cached := []

const CHUNKS_PER_ROW = 256
const CHUNKS_PER_TILESET = 64

var already_drawn := {}
var coordinate_metatile_properties := {}
#var tile_cache := []

func _ready():
	global_tileset = TileSet.new()

func create_chunk(column: int, row: int) -> void:
	if [column, row] in already_drawn:
		return

	var chunk: int
	var chunk_within_tileset: int
	var tileset_index: int
	var tile_changes: bool
	var sector_properties: Array
	if column < 0 or row < 0:
		# screen overflow
		# on the original game, this is handled by repeating a tile in the sector
		# the index is set by the palette, strangely enough
		# the only places that are possible to see this (I think) are buildings, which are all black
		chunk = main_map_rows[0][0]
		chunk_within_tileset = 0
		tileset_index = 0
		tile_changes = 0
		row = 0
		column = 0
		sector_properties = [1, 1, 0, 0]
		return # just return for now, I'll work on this later
	else:
		chunk = main_map_rows[row][column]
		chunk_within_tileset = chunk & 0x3f
		tileset_index = (chunk & 0x40) >> 6
		tile_changes = (chunk & 0x80) >> 7
		sector_properties = sectors[int(row / 4) * int(CHUNKS_PER_ROW / 4) + int(column / 4)]

	var chunk_object: Array = main_map_rows[tileset_index * CHUNKS_PER_TILESET + chunk_within_tileset]

	var main_alt_tileset := [sector_properties[1], sector_properties[2]]
	var tileset: int = main_alt_tileset[tileset_index]
	var alt_tileset: int = main_alt_tileset[~tileset_index] # bitwise NOT

	var palette = sector_properties[3]

	var new_chunk = Chunk.instance()
	new_chunk.set_parameters(global_tileset, global_tileset_cached, tileset, alt_tileset, chunk_within_tileset, palette)
	new_chunk.position = Vector2(column * 64, row * 64)
	add_child(new_chunk)

	already_drawn[[column, row]] = new_chunk
	#tile_cache.append([new_chunk, [column, row]]) # I add the column and row so it's faster to delete old ones
	var metatile_column: int
	var metatile_row: int
	var index := 0
	for metatile in chunk_object:
		metatile_column = column * 2 + index % 2
		metatile_row = row * 2 + index / 2
		#var new_text = Label.new()
		#new_text.text = str(metatile_column)
		#print(new_text.rect_position)
		#new_text.rect_position = Vector2(metatile_column * 16, metatile_row * 16)
		#add_child(new_text)
		coordinate_metatile_properties[[metatile_column, metatile_row]] = MapController.metatile_properties[metatile]
		index += 1

func clear() -> void:
	for value in already_drawn.values():
		value.queue_free()
	already_drawn = {}

func draw_chunks(chunks: Array, area = null) -> void:
	#print("chunks: array ", chunks)
	for coordinates in already_drawn.keys():
		if true:#not coordinates in chunks:
			var index := 0
			#for tile in tile_cache.duplicate():
			#	if tile[0] == already_drawn[coordinates]:
			#		tile_cache.erase(tile)
			#	index += 1
			already_drawn[coordinates].queue_free()
			already_drawn.erase(coordinates)
		elif area != null:
			var chunk_area: int = MapController.get_area(Vector2(coordinates[0] * CHUNKS_PER_TILESET, coordinates[1] * CHUNKS_PER_TILESET))
			if chunk_area != area:
				already_drawn[coordinates].queue_free()
				already_drawn.erase(coordinates)
	for chunk in chunks:
		#print("map ", MapController.get_area(Vector2(chunk[0] * CHUNKS_PER_TILESET, chunk[1] * CHUNKS_PER_TILESET)))
		if area != null and MapController.get_area(Vector2(chunk[0] * CHUNKS_PER_TILESET, chunk[1] * CHUNKS_PER_TILESET)) != area:
			continue
		create_chunk(chunk[0], chunk[1])
