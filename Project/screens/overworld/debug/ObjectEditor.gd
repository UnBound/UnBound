extends Control

signal update_properties(new_properties, node)

const HOLE = 4

func _set_properties(new_properties: Dictionary) -> void:
	properties = new_properties
	_reload_properties()
func _get_properties() -> Dictionary:
	return properties
var properties: Dictionary setget _set_properties, _get_properties
var node: Node
var tree_items: Array

# for door-like objects, set the direction to the OPPOSITE of the chosen option
# this makes the direction control where you must enter the door, which makes more sense
const DOOR_LIKE_DIRECTIONS = [4, 0, 2, 6]

func _ready() -> void:
	clear()

func load_object(new_properties: Dictionary, new_node: Node) -> void:
	properties = new_properties
	node = new_node
	_make_tree()
	_reload_properties()
	$Tree.show()
	print("editing ", new_properties)

func clear() -> void:
	$Tree.hide()

# reload the properties from the object and display them on the screen
func _reload_properties() -> void:
	if properties.type == HOLE and properties.direction != 0: # 
		properties.direction = 0
		emit_signal("update_properties", node, properties)
	for tree_item in tree_items:
		var property = tree_item.get_metadata(0)
		if property == "location_x":
			tree_item.set_range(1, properties.position_x)
		elif property == "location_y":
			tree_item.set_range(1, properties.position_y)
		elif property == "type":
			tree_item.set_range(1, properties.type - 1)
		elif property == "direction":
			tree_item.set_range(1, DOOR_LIKE_DIRECTIONS.find(properties.direction))

func _make_tree() -> void:
	tree_items.clear()
	$Tree.clear()
	var root: TreeItem = $Tree.create_item()

	var location_x: TreeItem = $Tree.create_item(root)
	location_x.set_metadata(0, "location_x")
	location_x.set_text(0, "X")
	location_x.set_cell_mode(1, TreeItem.CELL_MODE_RANGE)
	location_x.set_editable(1, true)
	location_x.set_range_config(1, 0, 65535, 1)
	tree_items.append(location_x)

	var location_y: TreeItem = $Tree.create_item(root)
	location_y.set_metadata(0, "location_y")
	location_y.set_text(0, "Y")
	location_y.set_cell_mode(1, TreeItem.CELL_MODE_RANGE)
	location_y.set_editable(1, true)
	location_y.set_range_config(1, 0, 65535, 1)
	tree_items.append(location_y)

	var type: TreeItem = $Tree.create_item(root)
	type.set_metadata(0, "type")
	type.set_text(0, "Type")
	type.set_cell_mode(1, TreeItem.CELL_MODE_RANGE)
	type.set_editable(1, true)
	type.set_text(1, "Door,Unused,Stairs,Hole") # it took me forever to figure out this is how Godot 3 does dropdowns
	type.set_tooltip(1, "How the game handles the object's properties")
	tree_items.append(type)

	if properties.type != HOLE: # holes don't have a direction
		var direction: TreeItem = $Tree.create_item(root)
		direction.set_metadata(0, "direction")
		direction.set_text(0, "Direction")
		direction.set_cell_mode(1, TreeItem.CELL_MODE_RANGE)
		direction.set_editable(1, true)
		direction.set_text(1, "Up,Down,Left,Right")
		direction.set_tooltip(1, "Which direction you can enter the door from")
		tree_items.append(direction)

# why is this called twice?
func _on_item_edited():
	# the metadata of the first column is used to determine the type
	var edited: TreeItem = $Tree.get_edited()
	var property = edited.get_metadata(0)
	if property == "location_x":
		properties.position_x = int(edited.get_range(1))
	elif property == "location_y":
		properties.position_y = int(edited.get_range(1))
	elif property == "type":
		properties.type = int(edited.get_range(1) + 1)
	elif property == "direction":
		properties.direction = DOOR_LIKE_DIRECTIONS[edited.get_range(1)]
	emit_signal("update_properties", node, properties)
	_reload_properties()
