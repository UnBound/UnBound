extends AcceptDialog

signal changed(x, y)

var changing := false

func set_value(x: int, y: int):
	# I use this so it doesn't emit a signal when you set the value
	changing = true
	$VBox/X/XInput.value = x
	$VBox/Y/YInput.value = y
	changing = false

func _on_value_changed() -> void:
	if not changing:
		emit_signal("changed", $VBox/X/XInput.value, $VBox/Y/YInput.value)

func _on_input_changed(_value: float) -> void:
	# this is just a wrapper for _on_value_changed because the signal has the value in it
	_on_value_changed()
