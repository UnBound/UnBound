extends Node2D

var ppu_tiles := MapController.ppu_tiles
var metatiles := MapController.metatiles
var chunks := MapController.chunks
var palettes := MapController.palettes
var metatile_properties := MapController.metatile_properties
var metatile_palettes := MapController.metatile_palettes

const CHUNKS_PER_TILESET = 64
const METATILES_PER_TILESET = 128

var tileset_index: int
var alt_tileset_index: int
var raw_palette: Array
func _get_palette_index() -> int:
	return palette_index
func _set_palette_index(new_palette_index: int) -> void:
	palette_index = new_palette_index
	raw_palette = palettes[palette_index]
var palette_index: int setget _set_palette_index, _get_palette_index
var chunk_index: int
var global_tileset: TileSet
var global_tileset_cached: Array

var _needs_update := false

func create_metatile(metatile: int) -> ImageTexture:
	var tile: Array = metatiles[metatile]
	var tileset: int = metatile / 128
	var result := ImageTexture.new()
	var image := Image.new()
	image.create(16, 16, false, Image.FORMAT_RGB8)
	image.lock()
	var pixel_row: int
	var pixel_column: int
	var row_offset: int
	var column_offset: int
	var pixel_index: int
	var metatile_index := 0
	for ppu_tile in tile:
		pixel_index = 0
		row_offset = int(metatile_index / 2) * 8
		column_offset = (metatile_index % 2) * 8
		for pixel in ppu_tiles[tileset * 64 + ppu_tile]:
			pixel_row = pixel_index / 8
			pixel_column = pixel_index % 8
			var num: int = MapController.metatile_palettes[tileset_index * 0x400 + chunk_index + 0x10 + metatile]
			print(metatile - tileset_index * METATILES_PER_TILESET)
			print(tileset_index * 0x400 + chunk_index + 0x10 + metatile, " / ", len(MapController.metatile_palettes))
			if num != 0:
				print(num)
			image.set_pixel(pixel_column + column_offset, pixel_row + row_offset, raw_palette[pixel])
			pixel_index += 1
		metatile_index += 1
	image.unlock()
	result.create_from_image(image, 0) # the zero is to remove the flags that do stupid anti-aliasing
	return result

func create_tileset(tileset) -> void:
	var square := RectangleShape2D.new()
	square.extents = Vector2(16, 16)
	if not (tileset in global_tileset_cached):
		for tile_id in range(METATILES_PER_TILESET * tileset, METATILES_PER_TILESET * (tileset + 1)):
			global_tileset.create_tile(tile_id)
			create_metatile(tile_id)

			# todo: this line causes a small performance drain and I don't know why, and it isn't from the create_metatile function
			# this causes walking to an area with a new tileset to freeze the game for a second
			# it's only called 256 times (one for the main and one for the alt), so it shouldn't be too taxing
			# later I can try to make things asynchronous
			global_tileset.tile_set_texture(tile_id, create_metatile(tile_id))

			var property: int = metatile_properties[tile_id]
			metatile_properties.append(property)
			if property != 0 and tile_id % 64 != 0:
				#print(property)
				global_tileset.tile_add_shape(tile_id, square, Transform2D.IDENTITY)
		global_tileset_cached.append(tileset)

func update() -> void:
	create_tileset(tileset_index)
	create_tileset(alt_tileset_index)
	_create_chunk(chunks[tileset_index * CHUNKS_PER_TILESET + chunk_index])

func _create_chunk(chunk: Array) -> void:
	var index := 0
	var row: int
	var column: int
	for metatile in chunk:
		row = index / 4
		column = index % 4
		if int(metatile / METATILES_PER_TILESET) == 0: # main tileset
			$Metatiles.set_cell(column, row, tileset_index * METATILES_PER_TILESET + metatile)
			# set the cell's palette here!!!!!
		else: # alternate tileset
			$Metatiles.set_cell(column, row, alt_tileset_index * METATILES_PER_TILESET + (metatile % METATILES_PER_TILESET))
		index += 1

func set_parameters(parameter_global_tileset: TileSet, parameter_global_tileset_cached: Array,
					tileset: int, alt_tileset: int, chunk: int, palette: int) -> void:
	if palette * 4 != 120:
		return
	global_tileset = parameter_global_tileset
	global_tileset_cached = parameter_global_tileset_cached
	tileset_index = tileset
	alt_tileset_index = alt_tileset
	chunk_index = chunk
	$Metatiles.tile_set = global_tileset
	_set_palette_index(palette * 4)
	update()

func _on_screen_exited() -> void:
	pass#$AutoFreeTimer.start(1)

func _on_screen_entered() -> void:
	$AutoFreeTimer.stop()

func _auto_free() -> void:
	queue_free()
