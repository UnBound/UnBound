extends KinematicBody2D

signal move(x, y)

export (int) var speed := 100
export (int) var run_speed := 300
var _current_speed := speed
var _target := Vector2.ZERO
var _current_target := Vector2.ZERO
var _old_target := Vector2.ZERO
var _snap := 16
var _animation_start: int
var _moving := false
# the OG time is every 8 frames at ~60 FPS = 133.33333...
# 150 is an approximation of that
var _animation_time := 150
var target_direction: int = GameController.DIRECTION_DOWN

# there aren't any diagonal sprites, so when moving diagonally, it just ignores the x value
const TARGET_DIRECTIONS = {
	Vector2(0, -1): GameController.DIRECTION_UP,
	Vector2(1, -1): GameController.DIRECTION_UP,
	Vector2(1, 0): GameController.DIRECTION_RIGHT,
	Vector2(1, 1): GameController.DIRECTION_DOWN,
	Vector2(0, 1): GameController.DIRECTION_DOWN,
	Vector2(-1, 1): GameController.DIRECTION_DOWN,
	Vector2(-1, 0): GameController.DIRECTION_LEFT,
	Vector2(-1, -1): GameController.DIRECTION_UP
}

func _ready() -> void:
	$Sprite.sprite = 0 # Ken
	$Sprite.direction = target_direction
	set_collision_mask_bit(2, false) # remove world collision layer for walk-through-walls
	_current_target = global_position

func _update_input() -> void:
	if Input.is_action_pressed("run"):
		_current_speed = run_speed
	else:
		_current_speed = speed
	_target = Vector2.ZERO
	if Input.is_action_pressed("up"):
		_target.y -= 1
	if Input.is_action_pressed("down"):
		_target.y += 1
	if Input.is_action_pressed("left"):
		_target.x -= 1
	if Input.is_action_pressed("right"):
		_target.x += 1
	if _target != Vector2.ZERO:
		target_direction = TARGET_DIRECTIONS[_target]
	if Input.is_physical_key_pressed(KEY_SLASH):
		position = Vector2.ZERO
		_current_target = global_position
	if Input.is_physical_key_pressed(KEY_0):
		layers = 0
	elif Input.is_physical_key_pressed(KEY_1):
		layers = 1

func _round_vector(vector: Vector2) -> Vector2:
	return vector.snapped(Vector2(1, 1))

var test := true

func check_collision(point: Vector2) -> bool:
	return GameController.collides(point)
	var ray_dir = point
	
	$RayCast.global_position = point + Vector2.ONE
	$RayCast.cast_to = Vector2.ONE * (_snap - 2)
	$RayCast/Debug.rect_size = Vector2.ONE * (_snap - 2)

	$RayCast.force_raycast_update()

	if $RayCast.is_colliding():
		return true
	else:
		return false

func _process(delta: float) -> void:
	_update_input()
	var old_position := global_position
	var rounded_position := Vector2(round(global_position.x), round(global_position.y))
	var new_moving := true
	if _target != _old_target:
		_old_target = Vector2.ZERO
	if rounded_position != _current_target:
		position = position.move_toward(_current_target, delta * _current_speed)
	elif _target != Vector2.ZERO and _target != _old_target:
		$Sprite.direction = target_direction
		var target_vector := rounded_position + _target * _snap
		var collided: bool
		# this avoids slight clipping through objects, but it turns out the original game doesn't have this
		# it's only visual and since most sprites are smaller then their hitbox, it's not very noticeable
		#if _target.x != 0 and _target.y != 0: # if it's diagonal, I need to check both directions to avoid clipping
		#	var target_1 := Vector2(_target.x, 0)
		#	var target_2 := Vector2(0, _target.y)
		#	collided = GameController.collides(global_position + target_1 * _snap)\
		#				or GameController.collides(global_position + target_2 * _snap)\
		#				or GameController.collides(global_position + _target * _snap)
		#else:
		#	collided = GameController.collides(global_position + _target * _snap)
		collided = GameController.collides(global_position + _target * _snap)
		if collided:
			new_moving = false
			_old_target = _target # prevent collision checking every frame
		else:
			_current_target = Vector2(stepify(target_vector.x, _snap), stepify(target_vector.y, _snap))
	else:
		global_position = rounded_position
		new_moving = false
		$Sprite.animation = 0 # when you stop moving, this moves the feet back to their original position, which is what the game does

	if new_moving and not _moving: # if the sprite just started moving after being stationary...
		_animation_start = OS.get_ticks_msec() # ...start the animation
	_moving = new_moving
	if _moving:
		if ((OS.get_ticks_msec() - _animation_start) % (_animation_time * 2)) < _animation_time:
			$Sprite.animation = 1
		else:
			$Sprite.animation = 0
	if old_position != global_position:
		emit_signal("move", position.x, position.y)

var test_one := 0
var test_two := 0

func move_to_without_emit(x: int, y: int) -> bool:
	var new_position := Vector2(x, y)
	var changed := new_position != position # checked so I can emit a signal if my position changed
	position = new_position
	_target = new_position
	_current_target = new_position
	return changed

func move_to(x: int, y: int) -> void:
	if move_to_without_emit(x, y):
		emit_signal("move", position.x, position.y)
