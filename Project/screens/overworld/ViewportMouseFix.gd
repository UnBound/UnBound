extends ViewportContainer

var already_sent := []

func _input(event):
	# the viewport stretching causes problems with mouse processing unless I manually calculate it
	# partially based on the fix by ArdaE https://github.com/godotengine/godot/issues/17326#issuecomment-431186323
	if event in already_sent:
		already_sent.erase(event)
		return
	if event is InputEventMouse:
		if get_rect().has_point(event.position): # if input is inside the viewport
			var new_event: InputEventMouse = event.duplicate()
			var stretch_amount: Vector2 = rect_size / $Viewport.get_visible_rect().size
			var position := Vector2(margin_left, margin_top)
			new_event.position = (event.position - position) / stretch_amount
			$Viewport.input(new_event)
			already_sent.append(event) # to avoid the next line causing infinite recursion
			get_viewport().input(event) # send the old event to other objects
			$Viewport.set_input_as_handled()
