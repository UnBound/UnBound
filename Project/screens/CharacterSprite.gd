extends AnimatedSprite

func _set_character_name(new_name: String) -> void:
	character_name = new_name
	_update_character()
func _get_character_name() -> String:
	return character_name

var character_name: String setget _set_character_name, _get_character_name

func _ready() -> void:
	_update_character()

func _update_character() -> void:
	if character_name == "":
		return
	frames = SpriteFrames.new()
	var index := 0
	for animation_name in ["back", "right", "front", "left", "backright", "frontright", "frontleft", "backleft"]:
		frames.add_animation(animation_name)
		frames.add_frame(animation_name, load("res://sprites/characters/%s%d.png" % [character_name, index * 2]))
		frames.add_frame(animation_name, load("res://sprites/characters/%s%d.png" % [character_name, index * 2 + 1]))
		index += 1
	animation = "front"
